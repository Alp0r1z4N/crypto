//
//  aes.h
//  gcm
//
//  Created by Tristan Bertin on 16/06/15.
//  Copyright (c) 2015 Tristan Bertin. All rights reserved.
//

#ifndef gcm_aes_h
#define gcm_aes_h

#define uchar unsigned char
#define uint unsigned int


void AddRoundKey(uchar state[][4], uchar w[]);
void SubBytes(uchar state[][4]);
void InvSubBytes(uchar state[][4]);
void ShiftRows(uchar state[][4]);
void InvShiftRows(uchar state[][4]);
void MixColumns(uchar state[][4]);
void InvMixColumns(uchar state[][4]);
void printstate(uchar state[][4]);
void print_rnd_key(uint key[]);
uint SubWord(uint word);
void KeyExpansion(uchar key[], uint w[], int keysize);
void aes_encrypt(uchar in[], uchar out[], uchar key[], int keysize);
void aes_decrypt(uchar in[], uchar out[], uchar key[], int keysize);



#endif
